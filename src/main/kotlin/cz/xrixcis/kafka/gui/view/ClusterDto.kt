package cz.xrixcis.kafka.gui.view

import javax.validation.constraints.NotBlank

data class ClusterDto(@field:NotBlank val name: String?, @field:NotBlank val zkUrl: String?) {
}
