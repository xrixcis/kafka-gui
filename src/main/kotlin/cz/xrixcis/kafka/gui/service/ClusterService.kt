package cz.xrixcis.kafka.gui.service

import cz.xrixcis.kafka.gui.model.Cluster
import cz.xrixcis.kafka.gui.view.ClusterDto
import java.util.*

interface ClusterService {

    fun getClusters(): List<Cluster>

    fun getCluster(id: Long): Optional<Cluster>

    fun saveCluster(cluster: ClusterDto): Cluster

    fun deleteCluster(clusterId: Long)
}
