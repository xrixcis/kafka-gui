package cz.xrixcis.kafka.gui.controller

import cz.xrixcis.kafka.gui.NotFoundException
import cz.xrixcis.kafka.gui.model.Cluster
import cz.xrixcis.kafka.gui.service.ClusterService
import cz.xrixcis.kafka.gui.view.ClusterDto
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/cluster")
class ClusterController(private val service: ClusterService){

    @GetMapping
    fun getAll() = service.getClusters()

    @GetMapping("/{id}")
    fun get(@PathVariable("id") id: Long): Cluster = service.getCluster(id).orElseThrow { NotFoundException() }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody @Valid cluster: ClusterDto) = service.saveCluster(cluster)

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable("id") id: Long) = service.deleteCluster(id)
}
