package cz.xrixcis.kafka.gui.service.impl

import cz.xrixcis.kafka.gui.model.Cluster
import cz.xrixcis.kafka.gui.repository.ClusterRepository
import cz.xrixcis.kafka.gui.service.ClusterService
import cz.xrixcis.kafka.gui.view.ClusterDto
import org.springframework.stereotype.Service
import java.util.*

@Service
class ClusterServiceImpl(private val repository: ClusterRepository) : ClusterService {

    override fun getClusters(): List<Cluster> {
        return repository.findAll().toList()
    }

    override fun getCluster(id: Long): Optional<Cluster> {
        return repository.findById(id)
    }

    override fun saveCluster(cluster: ClusterDto): Cluster {
        return repository.save(Cluster(null, cluster.name!!, cluster.zkUrl!!))
    }

    override fun deleteCluster(clusterId: Long) {
        repository.deleteById(clusterId)
    }
}
