package cz.xrixcis.kafka.gui.model

import org.springframework.data.annotation.Id

data class Cluster(@Id val id: Long?, val name: String, val zkUrl: String) {

}
