package cz.xrixcis.kafka.gui

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.socket.config.annotation.EnableWebSocket

@SpringBootApplication
@EnableJdbcRepositories
@EnableWebMvc
@EnableKafka
@EnableWebSocket
class KafkaGuiApplication

fun main(args: Array<String>) {
    runApplication<KafkaGuiApplication>(*args)
}
