package cz.xrixcis.kafka.gui.repository

import cz.xrixcis.kafka.gui.model.Cluster
import org.springframework.data.jdbc.repository.query.Modifying
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

interface ClusterRepository : CrudRepository<Cluster, Long> {

}
